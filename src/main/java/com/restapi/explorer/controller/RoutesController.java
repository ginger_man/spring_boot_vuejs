package com.restapi.explorer.controller;

import java.io.IOException;
import java.sql.Time;
import java.util.*;

import com.google.gson.Gson;
import com.google.maps.model.LatLng;
import com.restapi.explorer.model.PIKRotes;
import com.restapi.explorer.model.Routes;

import com.restapi.explorer.repo.RouteRepository;
import com.restapi.explorer.repo.RoutesPIKRepository;

import com.restapi.explorer.travel.lanch.GetInfoGoogle;
import com.restapi.explorer.travel.lanch.PIK.Parse.ParsePIK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.crypto.Data;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class RoutesController {

	@Autowired
	RouteRepository repository;

	@Autowired
	RoutesPIKRepository repositoryPIK;

    @GetMapping("/cleanAll")
    public void cleanAllDB() {
        repositoryPIK.deleteAll();
        repository.deleteAll();
    }

    @GetMapping("/markersInfo")
    public List<PIKRotes> markersInfo(String route) {
        System.out.println(route);
        List<PIKRotes> pikRoutes = new ArrayList<>();
        repositoryPIK.findByRoute(route).forEach(pikRoutes::add);
        System.out.println(pikRoutes);
        return pikRoutes;
    }

	@GetMapping("/markers")
	public List<Routes> getAllMarkers() {
		List res = new ArrayList();
        Long time =new Date().getTime();
		System.out.println("Get all routes... " + time);

		List<Routes> routes = new ArrayList<>();
		repository.findAll().forEach(routes::add);
        System.out.println(routes);

		return routes;
	}

	@PostMapping("/route")
	public Routes postCustomer(@RequestBody Routes routes) {

		Routes _routes = repository.save(new Routes(routes.getRoute(), routes.getLng(), routes.getLat()));
		return _routes;
	}

	@DeleteMapping("/route/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable("id") long id) {
		System.out.println("Delete Routes with ID = " + id + "...");

		repository.deleteById(id);

		return new ResponseEntity<>("Routes has been deleted!", HttpStatus.OK);
	}

    @RequestMapping("/savetest")
    public void savetest() throws IOException {
        ArrayList<Routes> Clubs = new ArrayList<>();
        ArrayList<String> routesPIK = new ArrayList<>();
        ArrayList<String> titles = ParsePIK.getTitle();

        ArrayList<String> titlesTemp = new ArrayList<>();
   /*     HashMap<String, ArrayList<String[]>> resPIK = ParsePIK.getInfoRoute();
        // 0 - title; 1 - cost; 2 - link
        resPIK.forEach((k, v) -> {

            for(String[] temp : v) {
                long id;
                List<PIKRotes> tempRoute = repositoryPIK.findByLinkAndNameAndRoute(temp[2],temp[0],k);

                if(tempRoute.isEmpty()) {
                    repositoryPIK.save(new PIKRotes(k, temp[0], temp[2], temp[1], temp[3], temp[4]));
                }else{
                    String cost = tempRoute.get(0).getCost();
                    String costsale = tempRoute.get(0).getCostsale();
                    if(!cost.equals(temp[1])) {
                        temp[1] = cost;
                    }
                    if(!costsale.equals(temp[3])) {
                        temp[3] = costsale;
                    }
                    if(!cost.equals(temp[1])||!costsale.equals(temp[3])) {
                        id = tempRoute.get(0).getId();
                        repositoryPIK.save(new PIKRotes(id, k, temp[0], temp[2], temp[1], temp[3], temp[4]));
                        System.out.println("changed cost from "+cost+" to "+temp[1]+" "+temp[0]);
                    }
                    routesPIK.add(temp[0]);
                }

                //    System.out.println(temp[1] + temp[3] + temp[4] + " name -> " + temp[0] + " link -> " + temp[2]);
                //   System.out.println(tempRoute.isEmpty()?tempRoute:tempRoute.get(0).getName());

                //	routesPIK.add(new PIKRotes(k, temp[0], temp[2], temp[1], temp[3], temp[4]));
                // (String route, String name, String link, String cost,String costsale, String currency)
            }
        });*/

        System.out.println("PIK done");

        for (String titleStr: routesPIK) {
            if(repository.findByRoute(titleStr).isEmpty())
                titlesTemp.add(titleStr);
        }
        HashMap<String, String[]> res = GetInfoGoogle.getCoordinates(titles);
        System.out.println(res);
        if(!(res==null)) {
            res.forEach((k, v) -> {
                Clubs.add(new Routes(k, v[0], v[1]));
            });
            repository.saveAll(Clubs);
            System.out.println("google done");
        }else{
            System.out.println("All rotes in DB yet!");
        }
    }


    @RequestMapping("/save")
    public void process() throws IOException {
		ArrayList<Routes> Clubs = new ArrayList<>();
		ArrayList<PIKRotes> routesPIK = new ArrayList<>();
		ArrayList<String> titles = ParsePIK.getTitle();
        ArrayList<String> titlesTemp = new ArrayList<>();
		HashMap<String, ArrayList<String[]>> resPIK = ParsePIK.getInfoRoute();
		// 0 - title; 1 - cost; 2 - link
		resPIK.forEach((k, v) -> {

			for(String[] temp : v) {
			    long id;
                List<PIKRotes> tempRoute = repositoryPIK.findByLinkAndNameAndRoute(temp[2],temp[0],k);

                if(tempRoute.isEmpty()) {
                    repositoryPIK.save(new PIKRotes(k, temp[0], temp[2], temp[1], temp[3], temp[4]));
                }else{
                    String cost = tempRoute.get(0).getCost();
                    String costsale = tempRoute.get(0).getCostsale();
                    if(!cost.equals(temp[1])) {
                        temp[1] = cost;
                    }
                    if(!costsale.equals(temp[3])) {
                        temp[3] = costsale;
                    }
                    if(!cost.equals(temp[1])||!costsale.equals(temp[3])) {
                        id = tempRoute.get(0).getId();
                        repositoryPIK.save(new PIKRotes(id, k, temp[0], temp[2], temp[1], temp[3], temp[4]));
                        System.out.println("changed cost from "+cost+" to "+temp[1]+" "+temp[0]);
                    }
                }

            //    System.out.println(temp[1] + temp[3] + temp[4] + " name -> " + temp[0] + " link -> " + temp[2]);
             //   System.out.println(tempRoute.isEmpty()?tempRoute:tempRoute.get(0).getName());

			//	routesPIK.add(new PIKRotes(k, temp[0], temp[2], temp[1], temp[3], temp[4]));
               // (String route, String name, String link, String cost,String costsale, String currency)
			}
		});

		System.out.println("PIK done");

        for (String titleStr: titles) {
            if(repository.findByRoute(titleStr).isEmpty())
                titlesTemp.add(titleStr);
        }
		HashMap<String, String[]> res = GetInfoGoogle.getCoordinates(titlesTemp);
        System.out.println(res);
		if(!(res==null)) {
			res.forEach((k, v) -> {
				Clubs.add(new Routes(k, v[0], v[1]));
			});
			repository.saveAll(Clubs);
			System.out.println("google done");
		}else{
			System.out.println("All rotes in DB yet!");
		}
	}

        @GetMapping("routes/route/{route}")
	public List<Routes> findByRoute(@PathVariable String route) {

		List<Routes> routes = repository.findByRoute(route);
		return routes;
	}

	@PutMapping("/routes/{id}")
	public ResponseEntity<Routes> updateCustomer(@PathVariable("id") long id, @RequestBody Routes routes) {
		System.out.println("Update Routes with ID = " + id + "...");

		Optional<Routes> customerData = repository.findById(id);

		if (customerData.isPresent()) {
			Routes _routes = customerData.get();
			_routes.setRoute(routes.getRoute());
			_routes.setLng(routes.getLng());
			_routes.setLat(routes.getLat());
			return new ResponseEntity<>(repository.save(_routes), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
