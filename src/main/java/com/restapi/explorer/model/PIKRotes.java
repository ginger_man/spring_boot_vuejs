package com.restapi.explorer.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PIKRoutes")
public class PIKRotes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "Route")
	private String route;

	@Column(name = "name")
	private String name;

	@Column(name = "link")
	private String link;

	@Column(name = "cost")
	private String cost;

	@Column(name = "costsale")
	private String costsale;

	@Column(name = "currency")
	private String currency;


	public PIKRotes() {
	}

	public PIKRotes(String route, String name, String link, String cost,String costsale, String currency) {
		this.route = route;
		this.name = name;
		this.link = link;
		this.cost = cost;
		this.costsale = costsale;
		this.currency=currency;
	}

	public PIKRotes(long id, String route, String name, String link, String cost,String costsale, String currency) {
		this.id = id;
		this.route = route;
		this.name = name;
		this.link = link;
		this.cost = cost;
		this.costsale = costsale;
		this.currency=currency;
	}


	@Override
	public String toString() {
		return "TravelRoutes [id=" + id + ", route=" + route + ", cost=" + cost+", currency="+currency+"]";
	}
}
