package com.restapi.explorer.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TravelRoutes")
public class Routes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "Route")
	private String route;

	@Column(name = "lat")
	private String lat;

	@Column(name = "lng")
	private String lng;


	public Routes() {
	}

	public Routes(String route, String lat, String lng ) {
		this.route = route;
		this.lng = lng;
		this.lat = lat;
	}


	@Override
	public String toString() {
		return "{route:"+route+", lat:"+lat+", lng: "+lng+"}";
	}
}
