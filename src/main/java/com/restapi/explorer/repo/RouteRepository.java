package com.restapi.explorer.repo;

import java.util.List;

import com.restapi.explorer.model.Routes;
import org.springframework.data.repository.CrudRepository;

public interface RouteRepository extends CrudRepository<Routes, Long> {
	List<Routes> findByRoute(String route);
}
