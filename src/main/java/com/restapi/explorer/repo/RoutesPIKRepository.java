package com.restapi.explorer.repo;

import com.restapi.explorer.model.PIKRotes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoutesPIKRepository extends CrudRepository<PIKRotes, Long> {
	List<PIKRotes> findByRoute(String route);
	List<PIKRotes> findByLinkAndNameAndRoute(String link, String name, String route);

}
