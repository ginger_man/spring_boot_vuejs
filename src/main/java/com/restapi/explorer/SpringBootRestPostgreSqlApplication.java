package com.restapi.explorer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestPostgreSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestPostgreSqlApplication.class, args);
	}
}
