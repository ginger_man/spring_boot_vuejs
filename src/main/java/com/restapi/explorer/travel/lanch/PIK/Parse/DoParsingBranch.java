package com.restapi.explorer.travel.lanch.PIK.Parse;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;


public class DoParsingBranch {
    /**
     * @param uc - Document - DOM SITE
     * @return HashMap<Integer, ArrayList<String>>
     */
    public static ArrayList<String[]> getParsed(Document uc) {

         ArrayList<String[]> res = new ArrayList<>();
        Integer i = 0;
        ArrayList<String> tempRes = new ArrayList<>();

        Elements links = uc.select("div");

        for(Element obj: links) {
                String name = obj.attr("class");
                if("tour__card".equals(name)){
                    String[] tempInfo = new String[5];
                    Element link = obj.select("a").first();
                    Element divTitle = link.select("div").last();
                    Element p = obj.select("p").last();
                    tempInfo[0] = divTitle.text();
                    tempInfo[1] = p.ownText().replaceAll("\\D+","");                         ;
                    tempInfo[2] = link.attr("href");
                    tempInfo[3] = p.select("span").text().replaceAll("\\D+","");
                    tempInfo[4] = ""+ p.ownText().charAt(p.ownText().length() - 1);


                    res.add(tempInfo);
                }
            }

        return res;
    }
}
