package com.restapi.explorer.travel.lanch.PIK.Parse;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;


public class DoParsingMain {
    /**
     * @param uc - Document - DOM SITE
     * @return HashMap<Integer, ArrayList<String>>
     */
    public static HashMap<Integer, ArrayList<String>> getParsed(Document uc) {

        HashMap<Integer, ArrayList<String>> res = new HashMap<>();
        Integer i = 0;
        ArrayList<String> tempRes = new ArrayList<>();

        Elements links = uc.select("div");

        for(Element obj: links) {
            String name = obj.attr("class");
            if("links__column".equals(name)){
                Elements objLi = obj.select("li");
                for(Element objLis: objLi) {
                    Element link = objLis.select("a").first();

                    String hrefLi = link.attr("href");
                    String hrefText = link.text();

                    tempRes.add(hrefText);
                    tempRes.add(hrefLi);
                    tempRes = new ArrayList<>();

                    res.put(i,tempRes);

                    i++;
                }
            }
        }

/*        for (int z=0;z<res.size();z++) {
            if (!res.get(z).isEmpty()) {
                System.out.println(res.get(z).get(0));
                System.out.println(res.get(z).get(1));
            }
        }*/
        return res;
    }
}
