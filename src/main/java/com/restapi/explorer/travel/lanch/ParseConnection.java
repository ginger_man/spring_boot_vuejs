package com.restapi.explorer.travel.lanch;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class ParseConnection {

    public static Document connectSide(String url) throws IOException{
        Document doc = null;
        try {
                doc = Jsoup.connect(url).
//                        proxy("128.199.74.103", 8080).
                        timeout(10*3000).
                        validateTLSCertificates(false).
                        get();

        }catch(IOException e){
            System.out.println(e);
        }
        return doc;
}

}
