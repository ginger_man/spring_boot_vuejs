package com.restapi.explorer.travel.lanch;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class GetInfoGoogle {
    private static String URL_MAIN =  "https://maps.googleapis.com/maps/api/geocode/json?address=";
    private static String KEY_API_GOOGLE =  "&key=AIzaSyDcfinCkIuP-FG0J1k3YnhvjG5nFbQ6l2Q";


    /**
     * @param - Document - DOM SITE
     * @return HashMap<Integer, ArrayList<String>>
     */

    public static HashMap<String, String[]> getCoordinates(ArrayList<String> namesArea) throws IOException {

        HashMap<String, String[]> result = new HashMap<>();

        for (String str : namesArea) {
            try {
                URL url = new URL(URL_MAIN + str.replaceAll("[. ][+ ].+:", "+").replaceAll(" ", "+") + KEY_API_GOOGLE);
                System.out.println(url);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                System.out.println(con);
                con.setConnectTimeout(10 * 5000);
                con.setRequestMethod("GET");
                con.setRequestProperty("Content-Type", "application/json");

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                //Read JSON response and print
                    JSONObject myResponse = new JSONObject(response.toString());
                System.out.println(myResponse);
                    String statusJson = (String) myResponse.get("status");
                     if("OK".equals(statusJson)){
                    JSONArray arr = myResponse.getJSONArray("results");
                    JSONObject temp = arr.getJSONObject(0);
                    String formattedAdress = (String) temp.get("formatted_address");
                    String adressPlace = formattedAdress.substring(0,formattedAdress.contains(",")?formattedAdress.indexOf(','):formattedAdress.length());
                    JSONObject geometry = temp.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");

                    System.out.println(location.toString());
                         System.out.println(adressPlace);
                    String[] coordinates = new String[2];

                    coordinates[0] = location.optString("lng");
                    coordinates[1] = location.optString("lat");

                    result.put(str, coordinates);
                }else{
                    System.out.println("Response is Empty");
                     //   result.put(str, coordinates);
                }
                con.disconnect();
            }catch(IOException e){
                System.out.println(e);
            }
        }
        return result;
    }
}
