package com.restapi.explorer.travel.lanch.PIK.Parse;

import com.restapi.explorer.travel.lanch.*;
import org.jsoup.nodes.Document;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ParsePIK {

    public static HashMap<Integer, ArrayList<String>> getParedPIK() throws IOException {

        String mainUrl = "https://turclub-pik.ru";
        String url = mainUrl + "/search/";

        Document uc = ParseConnection.connectSide(url);

        //Main site parsing
        HashMap<Integer, ArrayList<String>> res = DoParsingMain.getParsed(uc);

        return res;
    }

    public static ArrayList<String> getTitle() throws IOException {

        ArrayList<String> titles = new ArrayList<>();

        HashMap<Integer, ArrayList<String>> res = getParedPIK();

        for (int z=0;z<res.size();z++) {
            if (!res.get(z).isEmpty()) {
                titles.add(res.get(z).get(0));
            }
        }
        return  titles;
    }

    public static HashMap<String, ArrayList<String[]>> getInfoRoute() throws IOException {

        HashMap<String, ArrayList<String[]>> resMap = new HashMap<>();

        String mainUrl = "https://turclub-pik.ru";
        String url = mainUrl + "/search/";

        Document uc = ParseConnection.connectSide(url);

        //Main site parsing
        HashMap<Integer, ArrayList<String>> res = DoParsingMain.getParsed(uc);

        ArrayList<String> titles = new ArrayList<>();

        //Route branches parsing
        for (int z=0;z<res.size();z++) {
            if (!res.get(z).isEmpty()) {
                Document route = ParseConnection.connectSide(mainUrl+res.get(z).get(1));
                ArrayList<String> titleRoute = new ArrayList<>();
                titleRoute.add(res.get(z).get(0));
                titles.add(res.get(z).get(0));

                if(route!=null) {
                    ArrayList<String[]> resBranch;
                    resBranch = DoParsingBranch.getParsed(route);
                    resMap.put(res.get(z).get(0),resBranch);

                   }
                }
            }

        return  resMap;
    }

}
