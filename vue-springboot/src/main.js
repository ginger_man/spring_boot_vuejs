import Vue from "vue";
import App from "./App.vue";
import router from './router';
import * as VueGoogleMaps from 'vue2-google-maps'

/*Vue.config.productionTip = false;*/

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDcfinCkIuP-FG0J1k3YnhvjG5nFbQ6l2Q',
        libraries: 'places', // This is required if you use the Autocomplete plugin,
    },
});



new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
